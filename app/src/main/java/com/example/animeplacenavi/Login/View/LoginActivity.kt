package com.example.animeplacenavi.Login.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.animeplacenavi.Login.Model.UserRepository
import com.example.animeplacenavi.Login.Presenter.LoginContact
import com.example.animeplacenavi.Login.Presenter.LoginPresenter
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginContact.View {
    private var presenter: LoginPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter = LoginPresenter(this, this, UserRepository())
        adminLogin()
        guestLogin()

    }

    private fun guestLogin() {
        guestBtn.setOnClickListener {
            val mPrefs = this?.getSharedPreferences("userTypeValue", 0)
            mPrefs?.edit()?.clear()?.apply()
            val intent = Intent().setClassName(
                "com.example.animeplacenavi",
                "com.example.animeplacenavi.Home.View.MainActivity"
            )
            startActivity(intent)
        }
    }

    private fun adminLogin() {

        loginButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            presenter?.login(email, password)
        }
    }

    override fun movePageToHome(intent: Intent) {
        Toast.makeText(this, "login success", Toast.LENGTH_SHORT).show()
        startActivity(intent)
    }

    override fun loginFail(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}