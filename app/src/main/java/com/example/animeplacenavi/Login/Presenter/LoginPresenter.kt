package com.example.animeplacenavi.Login.Presenter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.example.animeplacenavi.Login.Model.User
import com.example.animeplacenavi.Login.Model.UserRepository

class LoginPresenter(val context: Context, val loginView: LoginContact.View, val repository: UserRepository): LoginContact.Presenter {
    override fun login(email: String, password: String) {
        val userData = User(email, password,"")
        repository.verifyLogin(userData, this)
    }

    override fun userDataValid(userType: String) {
        val sharePreference : SharedPreferences = context.getSharedPreferences("userTypeValue", 0)
        val intent = Intent().setClassName("com.example.animeplacenavi", "com.example.animeplacenavi.Home.View.MainActivity")
        val editor = sharePreference.edit()
        editor.putString("userType", userType)
        editor.apply()
        editor.commit()
        loginView.movePageToHome(intent)
    }

    override fun userDataInValid(message: String) {
        loginView.loginFail(message)
    }

}