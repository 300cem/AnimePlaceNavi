package com.example.animeplacenavi.Login.Presenter

import android.content.Intent
import com.example.animeplacenavi.Login.Model.User

interface LoginContact {

    interface Model {
        fun verifyLogin(user: User, presenter: LoginPresenter)
    }

    interface Presenter {
        fun login(email: String, password: String)
        fun userDataValid(userType: String)
        fun userDataInValid(message: String)
    }

    interface View {
        fun movePageToHome(intent: Intent)
        fun loginFail(message: String)
    }

}