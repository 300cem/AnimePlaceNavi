package com.example.animeplacenavi.Login.Helper

import retrofit2.Call
import com.example.animeplacenavi.Login.Model.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginServices {
    @POST("/auth")
    fun loginRequest(@Body user: User): Call<User>
    @GET("/auth")
    fun testlogin(): Call<User>
}