package com.example.animeplacenavi.Login.Model

data class User(val email: String, val password: String, val userType: String)