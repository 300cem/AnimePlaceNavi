package com.example.animeplacenavi.Login.Model

import com.example.animeplacenavi.Login.Helper.LoginApi
import com.example.animeplacenavi.Login.Helper.LoginServices
import com.example.animeplacenavi.Login.Presenter.LoginContact
import com.example.animeplacenavi.Login.Presenter.LoginPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository: LoginContact.Model {
    override fun verifyLogin(user: User, presenter: LoginPresenter) {
        println("verifyLogin  ${user.email} , ${user.password}")
        val apiClient = LoginApi.client.create(LoginServices::class.java)
        val call = apiClient?.loginRequest(user)
            call?.enqueue(object: Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                println("failed $t")
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                if(response.raw().code() != 200){
                    presenter.userDataInValid("wrong email or password")
                } else {
                    response.body()?.userType?.let { presenter.userDataValid(it) }
                }
            }
        })
    }
}