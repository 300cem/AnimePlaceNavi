package com.example.animeplacenavi.Login.View

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.fragment_user.view.*

class UserFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val userFragment = inflater.inflate(R.layout.fragment_user, container, false)
        val mPrefs = this.context?.getSharedPreferences("IDValue",0)
        val intent = Intent().setClassName("com.example.animeplacenavi", "com.example.animeplacenavi.Home.View.LoginActivity")
        userFragment?.logoutBtn?.setOnClickListener {
            // remove the userType storage
            mPrefs?.edit()?.clear()?.apply()
            startActivity(intent)
        }
        return userFragment
    }
}