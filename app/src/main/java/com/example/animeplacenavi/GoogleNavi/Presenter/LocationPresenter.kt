package com.example.animeplacenavi.GoogleNavi.Presenter

import android.content.Intent
import com.example.animeplacenavi.GoogleNavi.Model.Location
import com.example.animeplacenavi.GoogleNavi.Model.MapRepository

class LocationPresenter(private val locationView: LocationContact.View, val repository: MapRepository): LocationContact.Presenter {
    override fun getLocationData() {
        repository.loadResponse(this)
    }

    override fun getLocationIdData(_id: String, title: String, location: String, lat: String, lng: String, image: String) {
        val intent = Intent().setClassName("com.example.animeplacenavi", "com.example.animeplacenavi.GoogleNavi.View.UpdateLocationNaviActivity")
        intent.putExtra("naviId", _id)
        intent.putExtra("naviTitle", title)
        intent.putExtra("naviLocation", location)
        intent.putExtra("naviLat", lat)
        intent.putExtra("naviLng", lng)
        intent.putExtra("naviImage", image)
        locationView.moveToEditLocationPage(intent)
    }

    override fun getLocationResultSuccess(list: List<Location>?) {
        locationView.onLocationResultSuccess(list)
    }

    override fun getLocationResultFailure(error: Throwable) {
        TODO("Not yet implemented")
    }

    override fun deleteSelectedLocation(_id: String) {
        repository.deleteSelectedLocation(_id, this)
    }

    override fun deletedMessage(message: String) {
        locationView.refreshLocationListRecyclerView(message)
    }
}