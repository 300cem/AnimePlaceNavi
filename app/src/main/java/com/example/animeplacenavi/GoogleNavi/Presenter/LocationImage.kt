package com.example.animeplacenavi.GoogleNavi.Presenter

data class LocationImage(val title: String, val locationImage: String)