package com.example.animeplacenavi.GoogleNavi.Presenter

import com.example.animeplacenavi.GoogleNavi.Model.Location
import com.example.animeplacenavi.GoogleNavi.Model.MapRepository

class UpdateLocationPresenter(private val updateLocationView: LocationContact.updateLocationView, val repository: MapRepository): LocationContact.updateLocationNaviPresenter {
    override fun addLocation(title: String, locationName: String, lat: String, lng: String, image: String) {
        val locationData = Location(_id = "", title = title, locationName = locationName, lat = lat.toDouble(), lng = lng.toDouble(), image = image)
        repository.addLocation(locationData, this)
    }

    override fun addLocationResultSuccess(message: String) {
        updateLocationView.onUpdateLocationDataSuccess(message)
    }

    override fun addLocationResultFailure(error: Throwable) {
        TODO("Not yet implemented")
    }

    override fun updateLocation(_id: String, title: String, locationName: String, lat: String, lng: String, image: String) {
        val locationData = Location(_id = _id, title = title, locationName = locationName, lat = lat.toDouble(), lng = lng.toDouble(), image = image)
        repository.updateLocation(locationData, this)
    }

    override fun updateLocationResultSuccess(message: String) {
        updateLocationView.onUpdateLocationDataSuccess(message)
    }

    override fun updateLocationResultFailure(error: Throwable) {
        TODO("Not yet implemented")
    }
}