package com.example.animeplacenavi.GoogleNavi.View

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.animeplacenavi.GoogleNavi.Helper.LocationNaviAdapter
import com.example.animeplacenavi.GoogleNavi.Model.Location
import com.example.animeplacenavi.GoogleNavi.Model.MapRepository
import com.example.animeplacenavi.GoogleNavi.Presenter.LocationContact
import com.example.animeplacenavi.GoogleNavi.Presenter.LocationPresenter
import com.example.animeplacenavi.Home.View.CreateAnimeActivity
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.fragment_navi.view.*

class NaviFragment : Fragment(), LocationContact.View {
    private var presenter: LocationPresenter? = null
    private var locationNaviRecyclerView: RecyclerView?= null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val naviFragment = inflater.inflate(R.layout.fragment_navi, container, false)
        presenter = LocationPresenter(this, MapRepository())
        locationNaviRecyclerView = naviFragment?.findViewById(R.id.locationNaviRecyclerView)
        presenter?.getLocationData()
        naviFragment?.createNewAnimeDataBtn?.setOnClickListener {
            val intent = Intent(this.activity, CreateAnimeActivity::class.java)
            startActivity(intent)
        }
        naviFragment?.createNewNaviDataBtn?.setOnClickListener {
            val intent = Intent(this.activity, CreateLocationNaviActivity::class.java)
            startActivity(intent)
        }
        return naviFragment
    }

    override fun onLocationResultFailure(error: Throwable) {
        Toast.makeText(this.context, "Get Location data list error $error", Toast.LENGTH_SHORT).show()
    }

    override fun onLocationResultSuccess(list: List<Location>?) {
        val itemDecoration = DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
        val mDrawable = this.context?.let { ContextCompat.getDrawable(it,
            R.drawable.black_divider
        ) }
        mDrawable?.let { itemDecoration.setDrawable(it) }
        locationNaviRecyclerView?.addItemDecoration(itemDecoration)
        locationNaviRecyclerView?.hasFixedSize()
        val linearLayoutManager = LinearLayoutManager(this.context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        locationNaviRecyclerView?.layoutManager = linearLayoutManager
        val locationNaviAdapter = list?.let { presenter?.let { animenaviPresenter -> LocationNaviAdapter(it, animenaviPresenter) } }
        locationNaviRecyclerView?.adapter = locationNaviAdapter
    }

    override fun moveToEditLocationPage(intent: Intent) {
        startActivity(intent)
    }

    override fun refreshLocationListRecyclerView(message: String) {
        // refresh location data list when A item deleted
        onResume()
    }

    override fun onResume() {
        super.onResume()
        presenter?.getLocationData()
    }
}