package com.example.animeplacenavi.GoogleNavi.View

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import coil.api.load
import com.example.animeplacenavi.GoogleNavi.Model.MapRepository
import com.example.animeplacenavi.GoogleNavi.Presenter.LocationContact
import com.example.animeplacenavi.GoogleNavi.Presenter.UpdateLocationPresenter
import com.example.animeplacenavi.R
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_update_location_navi.*
import kotlinx.android.synthetic.main.activity_update_location_navi.latEditText
import kotlinx.android.synthetic.main.activity_update_location_navi.lngEditText
import kotlinx.android.synthetic.main.activity_update_location_navi.locationEditText
import kotlinx.android.synthetic.main.activity_update_location_navi.naviTitleEditText
import kotlinx.android.synthetic.main.activity_update_location_navi.selectedLocationImage
import kotlinx.android.synthetic.main.activity_update_location_navi.takePictureBtn
import java.io.File

private const val DEFAULT_FILE_NAME = "photo.jpg"
private const val REQUEST_CODE = 42
private var photoFile: File = File.createTempFile(DEFAULT_FILE_NAME, ".jpg")
class UpdateLocationNaviActivity : AppCompatActivity(), LocationContact.updateLocationView {
    private var imageName: String = ""
    private var presenter: UpdateLocationPresenter?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_location_navi)

        presenter = UpdateLocationPresenter(this, MapRepository())
        // get selected row's data from naviListActivity
        val bundle = intent.extras
        val naviId = bundle?.getString("naviId")
        val naviTitle = bundle?.getString("naviTitle")
        val naviLocation = bundle?.getString("naviLocation")
        val naviLat = bundle?.getString("naviLat")
        val naviLng = bundle?.getString("naviLng")
        val naviImage = bundle?.getString("naviImage")

        naviTitleEditText.setText(naviTitle)
        locationEditText.setText(naviLocation)
        latEditText.setText(naviLat)
        lngEditText.setText(naviLng)
        selectedLocationImage.load(naviImage)

        // take picture using camera
        takePictureBtn.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            photoFile = getPhotoFile(DEFAULT_FILE_NAME)
            val fileProvider = FileProvider.getUriForFile(
                this,
                "com.example.fileprovider", photoFile
            )
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)
            if (takePictureIntent.resolveActivity(this.packageManager) != null) {
                startActivityForResult(takePictureIntent, REQUEST_CODE)
            } else {
                Toast.makeText(this, "No permission to open Camera", Toast.LENGTH_SHORT).show()
            }
        }
        // if case
        println("default photo name = ${photoFile.name}")
        val defaultImage = photoFile.name
        updateNaviBtn.setOnClickListener {
            if (naviId != null && !photoFile.name.equals(defaultImage) ) {
                updateNaviDataNewImage(naviId)
            } else if (naviId != null && photoFile.name.equals(defaultImage) && naviImage != null) {
                updateNaviDataDefaultImage(naviId, naviImage)
            }
        }
    }

    private fun getPhotoFile(fileName: String) : File {
        val storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(fileName, ".jpg", storageDirectory)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val bitmap = BitmapFactory.decodeFile(photoFile.absolutePath)
            imageName = photoFile.absolutePath.toUri().lastPathSegment?.replace("photo.jpg","").toString()
            println("bitmap filename = $imageName")
            selectedLocationImage.setImageBitmap(bitmap)
            println("new image =  ${selectedLocationImage.drawable}")
        }
    }

    private fun updateNaviDataNewImage(naviId: String) {
        val pd = ProgressDialog(this)
        pd.window?.setBackgroundDrawable( ColorDrawable(Color.LTGRAY));

        pd.setTitle("Uploading")
        pd.show()

        // store image to the root path
        val imageRef = FirebaseStorage.getInstance().reference.child("/$imageName")
        val photoUrl = photoFile.toUri()
        imageRef.putFile(photoUrl)
            .addOnSuccessListener {
                pd.dismiss()
                Toast.makeText(applicationContext, "Image Uploaded", Toast.LENGTH_SHORT).show()
                storeLocationNaviDataToDB(naviId, imageRef)
            }
            .addOnFailureListener {
                pd.dismiss()
                Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
            }
    }

    private fun storeLocationNaviDataToDB(navid: String, imageRef: StorageReference) {
        imageRef.downloadUrl.addOnSuccessListener {downloadUrl ->
            println("download url = $downloadUrl")
            val title = naviTitleEditText.text.toString()
            val location = locationEditText.text.toString()
            val lat = latEditText.text.toString()
            val lng = lngEditText.text.toString()
            val image = downloadUrl.toString()
            presenter?.updateLocation(navid, title, location, lat, lng, image)
        }
    }
    private fun updateNaviDataDefaultImage(naviId: String, naviImage: String) {
        val title = naviTitleEditText.text.toString()
        val location = locationEditText.text.toString()
        val lat = latEditText.text.toString()
        val lng = lngEditText.text.toString()
        val image = naviImage
        presenter?.updateLocation(naviId, title, location, lat, lng, image)
    }

    override fun onUpdateLocationDataSuccess(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        val intent = Intent().setClassName(
            "com.example.animeplacenavi",
            "com.example.animeplacenavi.Home.View.MainActivity"
        )
        startActivity(intent)
    }

    override fun onUpdateLocationDataFailure(error: Throwable) {
        Toast.makeText(this, "Update location data error $error", Toast.LENGTH_SHORT).show()
    }
}