package com.example.animeplacenavi.GoogleNavi.View

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import coil.api.load
import com.example.animeplacenavi.GoogleNavi.Model.MapRepository
import com.example.animeplacenavi.GoogleNavi.Presenter.LocationImage
import com.example.animeplacenavi.GoogleNavi.Presenter.MapContact
import com.example.animeplacenavi.GoogleNavi.Presenter.MapPresenter
import com.example.animeplacenavi.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_anime_map_navi.*

class AnimeMapNaviActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener, MapContact.mapView {

    private lateinit var mMap: GoogleMap
    private lateinit var mapPresenter: MapPresenter
    private var mapRepository: MapRepository? = null
    var locationTitleImageData: LocationImage ?=null
    val locationImageList: ArrayList<LocationImage> = ArrayList<LocationImage>()
    private val LOCATION_PERMISSION_REQUEST = 1

    private fun getLocationAccess() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mMap.isMyLocationEnabled = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return
                }
                mMap.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    this,
                    "User has not granted location access permission",
                    Toast.LENGTH_LONG
                ).show()
                finish()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anime_map_navi)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapRepository = MapRepository()
        mapPresenter = MapPresenter(this, mapRepository)
        mapFragment.isAdded
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val bundle = intent.extras
        val naviTitle = bundle?.getString("naviTitle")
        println("navi Title from Map  $naviTitle")
        mMap = googleMap
        if(naviTitle != null) {
            mapPresenter.onMapReady(naviTitle)
        }
        getLocationAccess()
        mMap.setOnMarkerClickListener(this)

    }

    override fun addScooter(licensePlate: ArrayList<String>, locationArrayList: ArrayList<LatLng>, imageList: ArrayList<String>) {
        for (i in locationArrayList.indices) {
            mMap.addMarker(MarkerOptions().position(locationArrayList[i]).title(licensePlate[i]))
            locationTitleImageData = LocationImage(licensePlate[i],imageList[i])
            locationImageList.add(LocationImage(licensePlate[i], imageList[i]))
        }
    }

    override fun scrollMapTo(locationArrayList: ArrayList<LatLng>) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(locationArrayList[0].latitude,locationArrayList[0].longitude), 10F),5000,null)
    }

    override fun onMapViewResultError(error: Throwable) {
        Toast.makeText(this, "Get Location maker error $error", Toast.LENGTH_SHORT).show()
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        var currentLocationImage = ""
            for(i in locationImageList.indices) {
                if(marker?.title.equals(locationImageList[i].title)) {
                    currentLocationImage = locationImageList[i].locationImage
                }
            }
        locationImageView.load(currentLocationImage)
        return false
    }
}

