package com.example.animeplacenavi.GoogleNavi.Presenter

import android.content.Intent
import com.example.animeplacenavi.GoogleNavi.Model.Location

interface LocationContact {

    interface Model {
        fun loadResponse(presenter: LocationPresenter)
        fun addLocation(location: Location, presenter: UpdateLocationPresenter)
        fun updateLocation(location: Location, presenter: UpdateLocationPresenter)
        fun deleteSelectedLocation(_id: String, presenter: LocationPresenter)
    }

    interface Presenter {
        fun getLocationData()
        fun getLocationIdData(_id: String, title: String, location: String, lat: String, lng: String, image: String)
        fun getLocationResultSuccess(list: List<Location>?)
        fun getLocationResultFailure(error: Throwable)
        fun deleteSelectedLocation(_id: String)
        fun deletedMessage(message: String)
    }

    interface View {
        fun onLocationResultFailure(error: Throwable)
        fun onLocationResultSuccess(list: List<Location>?)
        fun moveToEditLocationPage(intent: Intent)
        fun refreshLocationListRecyclerView(message: String)
    }

    interface updateLocationNaviPresenter {
        fun addLocation(title: String, locationName: String, lat: String, lng: String, image: String)
        fun addLocationResultSuccess(message: String)
        fun addLocationResultFailure(error: Throwable)
        fun updateLocation(_id: String, title: String, locationName: String, lat: String, lng: String, image: String)
        fun updateLocationResultSuccess(message: String)
        fun updateLocationResultFailure(error: Throwable)
    }

    interface updateLocationView {
        fun onUpdateLocationDataSuccess(message: String)
        fun onUpdateLocationDataFailure(error: Throwable)
    }
}