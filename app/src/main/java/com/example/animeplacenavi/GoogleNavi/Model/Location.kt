package com.example.animeplacenavi.GoogleNavi.Model

data class Location(val _id: String, val title: String, val locationName: String, val lat: Double, val lng: Double, val image: String)