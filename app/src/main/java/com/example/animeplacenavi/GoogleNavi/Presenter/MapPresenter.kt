package com.example.animeplacenavi.GoogleNavi.Presenter

import com.example.animeplacenavi.GoogleNavi.Model.MapRepository
import com.google.android.gms.maps.model.LatLng

class MapPresenter(view: MapContact.mapView, val repository: MapRepository?): MapContact.Presenter {

    val mapMapView : MapContact.mapView ?= view
    override fun onMapReady(title: String) {
        repository?.loadLocation(this, title)
    }

    override fun getLocation(licensePlate: ArrayList<String>, locationList: ArrayList<LatLng>, locationImageList: ArrayList<String>) {
            mapMapView?.addScooter(licensePlate, locationList, locationImageList)
            mapMapView?.scrollMapTo(locationList)
    }

    override fun getLocationFailure(error: Throwable) {
        mapMapView?.onMapViewResultError(error)
    }
}