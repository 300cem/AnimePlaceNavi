package com.example.animeplacenavi.GoogleNavi.Presenter

import com.example.animeplacenavi.GoogleNavi.Model.Location
import com.google.android.gms.maps.model.LatLng

interface MapContact {
    interface Presenter {
        fun onMapReady(title: String)
        fun getLocation(licensePlate: ArrayList<String>, location: ArrayList<LatLng>, locationImage: ArrayList<String>)
        fun getLocationFailure(error: Throwable)
    }

    interface Model {
        fun loadLocation(presenter: MapPresenter, title: String)
    }

    interface mapView {
        fun addScooter(licensePlate: ArrayList<String>, location: ArrayList<LatLng>, locationImageList: ArrayList<String>)
        fun scrollMapTo(location: ArrayList<LatLng>)
        fun onMapViewResultError(error: Throwable)
    }
}