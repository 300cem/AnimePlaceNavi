package com.example.animeplacenavi.GoogleNavi.Helper

import com.example.animeplacenavi.GoogleNavi.Model.Location
import retrofit2.Call
import retrofit2.http.*

interface AnimeNaviServices {
    @GET("/navi")
    fun fetchAllNaviMaker() : Call<List<Location>>
    @GET("/navi/title")
    fun fetchAllNaviMakerByAnimeName(@Query ("title") title: String) : Call<List<Location>>
    @Headers("Content-Type: application/json")
    @POST("/navi")
    fun addNaviMaker(@Body location: Location): Call<Location>
    @PATCH("/navi/{naviId}")
    fun updateNaviMaker(@Path("naviId") _id: String, @Body location: Location) : Call<Location>
    // TODO: delete Navi method
    @DELETE("/navi/{naviId}")
    fun deleteNaviMaker(@Path("naviId") _id: String): Call<Location>
}