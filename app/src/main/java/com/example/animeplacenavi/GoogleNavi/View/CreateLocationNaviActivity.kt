package com.example.animeplacenavi.GoogleNavi.View

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import com.example.animeplacenavi.GoogleNavi.Model.MapRepository
import com.example.animeplacenavi.GoogleNavi.Presenter.LocationContact
import com.example.animeplacenavi.GoogleNavi.Presenter.UpdateLocationPresenter
import com.example.animeplacenavi.R
import com.google.android.material.internal.ContextUtils
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_create_location_navi.*
import java.io.File

private const val DEFAULT_FILE_NAME = "photo.jpg"
private const val REQUEST_CODE = 42
private lateinit var photoFile: File
class CreateLocationNaviActivity : AppCompatActivity(), LocationContact.updateLocationView {
    lateinit var imageName: String
    private var presenter: UpdateLocationPresenter?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_location_navi)
        presenter = UpdateLocationPresenter(this, MapRepository())
        takePictureBtn.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            photoFile = getPhotoFile(DEFAULT_FILE_NAME)
            val fileProvider = FileProvider.getUriForFile(this,
                "com.example.fileprovider", photoFile)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)
            if(takePictureIntent.resolveActivity(this.packageManager) != null) {
                startActivityForResult(takePictureIntent, REQUEST_CODE)
            } else {
                Toast.makeText(this, "No permission to open Camera", Toast.LENGTH_SHORT).show()
            }
        }
        createNaviBtn.setOnClickListener {
            createLocationNaviData()
        }
    }
    private fun getPhotoFile(fileName: String) : File {
        val storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(fileName, ".jpg", storageDirectory)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val bitmap = BitmapFactory.decodeFile(photoFile.absolutePath)
            imageName = photoFile.absolutePath.toUri().lastPathSegment?.replace("photo.jpg","").toString()
            println("bitmap filename = $imageName")
            selectedLocationImage.setImageBitmap(bitmap)
        }
    }

    private fun createLocationNaviData() {
        val pd = ProgressDialog(this)
        pd.window?.setBackgroundDrawable( ColorDrawable(Color.LTGRAY));

        pd.setTitle("Uploading")
        pd.show()

        // store image to the root path
        val imageRef = FirebaseStorage.getInstance().reference.child("/$imageName")
        val photoUrl = photoFile.toUri()
        imageRef.putFile(photoUrl)
            .addOnSuccessListener {
                pd.dismiss()
                Toast.makeText(applicationContext, "Image Uploaded", Toast.LENGTH_SHORT).show()
                storeLocationNaviDataToDB(imageRef)
            }
            .addOnFailureListener {
                pd.dismiss()
                Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
            }
    }

    private fun storeLocationNaviDataToDB(imageRef: StorageReference) {
        imageRef.downloadUrl.addOnSuccessListener {downloadUrl ->
            println("download url = $downloadUrl")
            val title = naviTitleEditText.text.toString()
            val location = locationEditText.text.toString()
            val lat = latEditText.text.toString()
            val lng = lngEditText.text.toString()
            val image = downloadUrl.toString()
            presenter?.addLocation(title, location, lat, lng, image)
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onUpdateLocationDataSuccess(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        getActivity(this)?.onBackPressed()
    }

    override fun onUpdateLocationDataFailure(error: Throwable) {
       Toast.makeText(this, "Create location data error $error", Toast.LENGTH_SHORT).show()
    }
}