package com.example.animeplacenavi.GoogleNavi.Model

import com.example.animeplacenavi.GoogleNavi.Helper.AnimeNaviApi
import com.example.animeplacenavi.GoogleNavi.Helper.AnimeNaviServices
import com.example.animeplacenavi.GoogleNavi.Presenter.*
import com.example.animeplacenavi.Home.Helper.AnimeApi
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MapRepository : MapContact.Model, LocationContact.Model {

    override fun loadLocation(presenter: MapPresenter, title: String) {
        val apiClient = AnimeNaviApi.client.create(AnimeNaviServices::class.java)
        val call = apiClient?.fetchAllNaviMakerByAnimeName(title)
        println("title = ${title}")
        call?.enqueue(object: Callback<List<Location>> {
            override fun onFailure(call: Call<List<Location>>, error: Throwable) {
                presenter.getLocationFailure(error)
            }

            override fun onResponse(call: Call<List<Location>>, response: Response<List<Location>>) {
                val result = response.body()
                val locationNameList = ArrayList<String>()
                val locationList = ArrayList<LatLng>()
                val locationImageList = ArrayList<String>()
                println("navi result successful = $result")
                if (result != null) {
                    for(i in result.indices) {
                        locationNameList.add(result[i].locationName)
                        locationList.add(LatLng(result[i].lat, result[i].lng))
                        locationImageList.add(result[i].image)
                    }
                }
                presenter.getLocation(locationNameList, locationList, locationImageList)
            }
        })
    }

    override fun loadResponse(presenter: LocationPresenter) {
        val apiClient = AnimeNaviApi.client.create(AnimeNaviServices::class.java)
        val call = apiClient?.fetchAllNaviMaker()
            call?.enqueue(object: Callback<List<Location>> {
                override fun onFailure(call: Call<List<Location>>, t: Throwable) {
                    println("navi result = $t")
                }

                override fun onResponse(call: Call<List<Location>>, response: Response<List<Location>>?) {
                    val list = response?.body()
                    presenter.getLocationResultSuccess(list)
                    println("list =  $list")
                }
            })
    }

    override fun addLocation(location: Location, presenter: UpdateLocationPresenter) {
        val apiClient= AnimeNaviApi.client.create(AnimeNaviServices::class.java)
        val call = apiClient?.addNaviMaker(location)
        call?.enqueue(object: Callback<Location> {
            override fun onFailure(call: Call<Location>, t: Throwable) {
                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call<Location>, response: Response<Location>) {
                presenter.addLocationResultSuccess("create location data successful")
            }
        })
    }

    override fun updateLocation(location: Location, presenter: UpdateLocationPresenter) {
        val apiClient = AnimeNaviApi.client.create(AnimeNaviServices::class.java)
        val call = apiClient?.updateNaviMaker(location._id, location)
        call?.enqueue(object: Callback<Location> {
            override fun onFailure(call: Call<Location>, t: Throwable) {
                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call<Location>, response: Response<Location>) {
                val result = response.body()
                println("update response body = $result")
                presenter.updateLocationResultSuccess("update location data successful")
            }
        })
    }

    override fun deleteSelectedLocation(_id: String, presenter: LocationPresenter) {
        val apiClient = AnimeNaviApi.client.create(AnimeNaviServices::class.java)
        val call = apiClient?.deleteNaviMaker(_id)
        call?.enqueue(object: Callback<Location> {
            override fun onFailure(call: Call<Location>, t: Throwable) {
                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call<Location>, response: Response<Location>) {
                val result = response.body()
                println("delete response body = $result")
                presenter.deletedMessage("delete $result successful")
            }
        })
    }
}