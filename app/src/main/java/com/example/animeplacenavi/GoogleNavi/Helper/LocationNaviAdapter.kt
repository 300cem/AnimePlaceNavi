package com.example.animeplacenavi.GoogleNavi.Helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.animeplacenavi.GoogleNavi.Model.Location
import com.example.animeplacenavi.GoogleNavi.Presenter.LocationPresenter
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.navi_list_row.view.*

class LocationNaviAdapter(private val animeNaviList: List<Location>, private val locationPresenter: LocationPresenter): RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val animeNaviView = LayoutInflater.from(parent.context).inflate(R.layout.navi_list_row, parent, false)
        return AnimeNaviViewHolder(animeNaviView, locationPresenter)
    }

    override fun getItemCount(): Int = animeNaviList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        animeNaviList[position].let { (holder as AnimeNaviViewHolder).bind(it) }

    }
    class AnimeNaviViewHolder(animeNaviView: View, private val locationPresenter: LocationPresenter) : RecyclerView.ViewHolder(animeNaviView) {
        fun bind(location: Location) {
            itemView.titleTextView.text = location.title
            itemView.locationNameTextView.text = location.locationName
            itemView.latTextView.text = location.lat.toString()
            itemView.lngTextView.text = location.lng.toString()

            itemView.editNaviBtn.setOnClickListener {
                locationPresenter.getLocationIdData(location._id, location.title, location.locationName, location.lat.toString(), location.lng.toString(), location.image)
            }
            itemView.deleteNaviBtn.setOnClickListener {
                locationPresenter.deleteSelectedLocation(location._id)
            }
        }
    }
}
