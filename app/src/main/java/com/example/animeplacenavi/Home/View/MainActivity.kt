package com.example.animeplacenavi.Home.View

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.animeplacenavi.Helper.NetworkCheckHelper
import com.example.animeplacenavi.GoogleNavi.View.NaviFragment
import com.example.animeplacenavi.R
import com.example.animeplacenavi.Login.View.UserFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment()
        val naviFragment = NaviFragment()
        val userFragment = UserFragment()
        // set Default Fragment
        setCurrentFragment(homeFragment)
        val mPrefs = this?.getSharedPreferences("userTypeValue",0)
        val userType = mPrefs?.getString("userType","").toString()

        if(userType != "admin") {
            bottomNavigationView.visibility = View.GONE
        }
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home ->setCurrentFragment(homeFragment)
                R.id.note ->setCurrentFragment(naviFragment)
                R.id.user ->setCurrentFragment(userFragment)
            }
            true
        }

    }
    // check mobile data / wifi is connected
    fun checkNetworkConnectionStatus() : String {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeInfo = connectivityManager.activeNetworkInfo
        val wifiConnected = activeInfo?.type == ConnectivityManager.TYPE_WIFI
        val mobileConnected = activeInfo?.type == ConnectivityManager.TYPE_MOBILE
        val networkCheckHelper = NetworkCheckHelper()
            networkCheckHelper.checkNetworkConnectionStatus(activeInfo, wifiConnected, mobileConnected)
            networkCheckHelper.observeNetworkStatus(connectivityManager)
        return networkCheckHelper.checkNetworkConnectionStatus(activeInfo, wifiConnected, mobileConnected)
    }

    private fun setCurrentFragment(fragment: Fragment)=
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flFragment,fragment)
            commit()
        }
}