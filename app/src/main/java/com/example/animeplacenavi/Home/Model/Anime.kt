package com.example.animeplacenavi.Home.Model

data class Anime(val _id: String, val title: String, val year: String, val image: String)