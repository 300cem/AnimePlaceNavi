package com.example.animeplacenavi.Home.Model

import com.example.animeplacenavi.Home.Helper.AnimeApi
import com.example.animeplacenavi.Home.Helper.AnimeServices
import com.example.animeplacenavi.Home.Presenter.AnimeContact
import com.example.animeplacenavi.Home.Presenter.AnimePresenter
import com.example.animeplacenavi.Home.Presenter.HomeContact
import com.example.animeplacenavi.Home.Presenter.HomePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AnimeRepository: HomeContact.Model, AnimeContact.Model {

    override fun loadResponse(homePresenter: HomePresenter) {
        val apiClient = AnimeApi.client.create(AnimeServices::class.java)
        val call = apiClient?.fetchAllRoute()
        call?.enqueue(object : Callback<List<Anime>> {
            override fun onFailure(call: Call<List<Anime>>, t: Throwable) {
                homePresenter.getAnimeResultFailure(t)
            }

            override fun onResponse(call: Call<List<Anime>>, response: Response<List<Anime>>) {
                val lists = response.body()
                homePresenter.getAnimeResultSuccess(lists)
            }
        })
    }

    override fun deleteSelectedAnime(_id: String, presenter: HomePresenter) {
        val apiClient = AnimeApi.client.create(AnimeServices::class.java)
        val call = apiClient?.deleteAnime(_id)
        call?.enqueue(object : Callback<Anime> {
            override fun onFailure(call: Call<Anime>, t: Throwable) {
                println("delte anime data failed $t")
            }

            override fun onResponse(call: Call<Anime>, response: Response<Anime>) {
                println("delete anime data , id = $_id")
                presenter.deletedMessage("Deleted an anime data")
            }
        })
    }

    override fun addAnime(anime: Anime, presenter: AnimePresenter) {
        val apiClient = AnimeApi.client.create(AnimeServices::class.java)
        val call = apiClient?.addAnime(anime)
        call?.enqueue(object : Callback<Anime> {
            override fun onFailure(call: Call<Anime>, t: Throwable) {
                presenter.addAnimeResultFailure(t)
            }

            override fun onResponse(call: Call<Anime>, response: Response<Anime>) {
                presenter.addAnimeResultSuccess("add anime  successful ")
            }
        })
    }

    override fun updateAnime(anime: Anime, presenter: AnimePresenter) {
        val apiClient = AnimeApi.client.create(AnimeServices::class.java)
        val call = apiClient?.updateAnime(anime._id, anime)
        call?.enqueue(object : Callback<Anime> {
            override fun onFailure(call: Call<Anime>, t: Throwable) {
                presenter.updateAnimeResultFailure(t)
            }

            override fun onResponse(call: Call<Anime>, response: Response<Anime>) {
                presenter.updateAnimeResultSuccess("update anime successful")
            }
        })
    }
}