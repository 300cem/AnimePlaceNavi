package com.example.animeplacenavi.Home.View

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import com.example.animeplacenavi.Home.Model.AnimeRepository
import com.example.animeplacenavi.Home.Presenter.AnimeContact
import com.example.animeplacenavi.Home.Presenter.AnimePresenter
import com.example.animeplacenavi.R
import com.google.android.material.internal.ContextUtils
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_create_anime.*

class CreateAnimeActivity : AppCompatActivity(), AnimeContact.View {

    lateinit var filePath: Uri
    private var presenter: AnimePresenter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_anime)
        presenter = AnimePresenter(this, AnimeRepository())
        addAnimeBtn.setOnClickListener {
            addAnimeData()
        }

        selectImageBtn.setOnClickListener {
            startFileChooser()
        }
    }
    private fun startFileChooser() {
        val i = Intent()
        // * -> all types of image
        i.type = "image/*"
        i.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(i, "Choose Image"), 111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 111 &&resultCode == Activity.RESULT_OK && data != null) {

            filePath = data.data!!
            var bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            selectedImage.setImageBitmap(bitmap)
        } else {
            println("onActivityResult result code failed = $resultCode")
        }
    }

    private fun addAnimeData() {
        val pd = ProgressDialog(this)
        pd.window?.setBackgroundDrawable( ColorDrawable(Color.LTGRAY));

        pd.setTitle("Uploading")
        pd.show()

        val imageName = filePath.lastPathSegment
        // store image to the root path
        val imageRef = FirebaseStorage.getInstance().reference.child("/$imageName")
        imageRef.putFile(filePath)
                .addOnSuccessListener {
                    pd.dismiss()
                    Toast.makeText(applicationContext, "Image Uploaded", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    pd.dismiss()
                    Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
                }
        // get current uploaded image url using downloadUrl method
        imageRef.downloadUrl.addOnSuccessListener {downloadUrl ->
            println("url = $downloadUrl")
            val title = titleEditText.text.toString()
            val year = yearEditText.text.toString()
            val image = downloadUrl.toString()
            presenter?.addAnime(title, year, image)
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onAnimeResultSuccess(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        val intent = Intent().setClassName(
            "com.example.animeplacenavi",
            "com.example.animeplacenavi.Home.View.MainActivity"
        )
        startActivity(intent)
    }

    override fun onAnimeResultFailure(error: Throwable) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
    }
}