package com.example.animeplacenavi.Home.Helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.animeplacenavi.Home.Model.Anime
import com.example.animeplacenavi.Home.Presenter.HomePresenter
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.news_item.view.*

class NewsAdapter(private val animeList: List<Anime>, private val homePresenter: HomePresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val animeView = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return AnimeRouteViewHolder(animeView, homePresenter)
    }

    override fun getItemCount(): Int = animeList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        animeList[position]?.let { (holder as AnimeRouteViewHolder).bind(it) }
    }

    class AnimeRouteViewHolder(animeView: View, private val homePresenter: HomePresenter) : RecyclerView.ViewHolder(animeView) {
        fun bind(anime: Anime) {
            itemView.animeNewsImageView.load(anime.image)
            itemView.setOnClickListener {
                homePresenter.getAnimeTitle(anime.title)
            }
        }
    }
}