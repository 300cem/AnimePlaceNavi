package com.example.animeplacenavi.Home.Presenter

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.view.Gravity
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat.getSystemService
import com.example.animeplacenavi.Helper.GravitySnapHelper
import com.example.animeplacenavi.Helper.NetworkCheckHelper
import com.example.animeplacenavi.Home.Helper.AnimeAdapter
import com.example.animeplacenavi.Home.Helper.NewsAdapter
import com.example.animeplacenavi.Home.Model.Anime
import com.example.animeplacenavi.Home.Model.AnimeRepository

class HomePresenter(val context: Context, val homeView : HomeContact.View, val repository: AnimeRepository): HomeContact.Presenter {
    var isAdmin : Boolean = false
    val NET_WORK_CONNECTED = "Network connected"
    val NET_WORK_NOT_CONNECTED = "Network not connected"
    override fun checkNetworkStatus(status: String) {
        if (status == NET_WORK_CONNECTED) {
            println("network status is connected in fragment")
            homeView.onNetworkStatusResult(status)
        }
    }

    override fun callJson() {
        repository.loadResponse(this)
    }

    override fun getAnimeResultSuccess(list: List<Anime>?) {
        homeView.onAnimeResultSuccess(list)
    }

    override fun getAnimeResultFailure(error: Throwable) {
        homeView.onAnimeResultFailure(error)
    }

    override fun getScrollPosition(scrollPosition: Int, adapter: NewsAdapter) {
        val customSnapHelperStart = GravitySnapHelper(Gravity.START)
        val customSnapHelperEnd = GravitySnapHelper(Gravity.END)
        when (scrollPosition) {
            0 -> homeView.onScrollToPosition(customSnapHelperStart)
            adapter.itemCount - 1 -> homeView.onScrollToPosition(customSnapHelperEnd)
        }

    }

    override fun getAnimeId(_id: String, title: String, year: String, image: String) {
        val intent = Intent().setClassName("com.example.animeplacenavi","com.example.animeplacenavi.Home.View.UpdateAnimeActivity")
        intent.putExtra("animeId", _id)
        intent.putExtra("animeTitle", title)
        intent.putExtra("animeYear", year)
        intent.putExtra("animeImage", image)
        homeView.moveToEditAnimePage(intent)
    }

    override fun deleteSelectedAnime(_id: String) {
        repository.deleteSelectedAnime(_id, this)
    }

    override fun deletedMessage(message: String) {
        homeView.refreshAnimeListRecyclerView(message)
    }

    override fun getAnimeTitle(title: String) {
        val intent = Intent().setClassName("com.example.animeplacenavi", "com.example.animeplacenavi.GoogleNavi.View.AnimeMapNaviActivity")
        intent.putExtra("naviTitle", title)
        homeView.moveToAnimeAnimeNaviMapPage(intent)
    }

    override fun checkUserType(userType: String) {
        if(userType == "admin") {
            isAdmin = true
        }
    }

    override fun buttonVisible(editAnimeBtn: Button?, deleteAnimeBtn: Button?) {
        if (!isAdmin) {
            editAnimeBtn?.visibility = View.GONE
            deleteAnimeBtn?.visibility = View.GONE
        }
    }

    override fun animeFilter(title: String, animeList: List<Anime>?, animeAdapter: AnimeAdapter?) {
        val filteredList = ArrayList<Anime>()
        if (animeList != null) {
            for (anime in animeList) {
                if(anime.title.toLowerCase().contains(title.toLowerCase())) {
                    filteredList.add(anime)
                }
                animeAdapter?.filterAnime(filteredList)
            }
        }
    }
}