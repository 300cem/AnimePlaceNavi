package com.example.animeplacenavi.Home.Presenter

import com.example.animeplacenavi.Home.Model.Anime
import com.example.animeplacenavi.Home.Model.AnimeRepository

class AnimePresenter(val animeView: AnimeContact.View, val repository: AnimeRepository): AnimeContact.Presenter {
    override fun addAnime(title: String, year: String, image: String) {
        val animeData = Anime(_id = "",
                                  title = title,
                                  year = year,
                                  image = image)
        repository.addAnime(animeData, this)
    }

    override fun updateAnime(_id: String, title: String, year: String, image: String) {
        val animeData = Anime(_id = _id,
            title = title,
            year = year,
            image = image)
        repository.updateAnime(animeData, this)
    }

    override fun addAnimeResultSuccess(message: String) {
        animeView.onAnimeResultSuccess(message)
    }

    override fun addAnimeResultFailure(error: Throwable) {
       animeView.onAnimeResultFailure(error)
    }

    override fun updateAnimeResultSuccess(message: String) {
        animeView.onAnimeResultSuccess(message)
    }

    override fun updateAnimeResultFailure(error: Throwable) {
        animeView.onAnimeResultFailure(error)
    }
}