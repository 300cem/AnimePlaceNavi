package com.example.animeplacenavi.Home.Presenter

import com.example.animeplacenavi.Home.Model.Anime

interface AnimeContact {

    interface Model {
        fun addAnime(anime: Anime, presenter: AnimePresenter)
        fun updateAnime(anime: Anime, presenter: AnimePresenter)
    }

    interface Presenter {
        fun addAnime(title: String, year: String, image: String)
        fun updateAnime(_id: String, title: String, year: String, image: String)
        fun addAnimeResultSuccess(message: String)
        fun addAnimeResultFailure(error: Throwable)
        fun updateAnimeResultSuccess(message: String)
        fun updateAnimeResultFailure(error: Throwable)
    }

    interface View {
        fun onAnimeResultSuccess(message: String)
        fun onAnimeResultFailure(error: Throwable)
    }
}