package com.example.animeplacenavi.Home.Helper

import com.example.animeplacenavi.Home.Model.Anime
import retrofit2.Call
import retrofit2.http.*

interface AnimeServices {
    @GET("/animes")
    fun fetchAllRoute() : Call<List<Anime>>
    @Headers("Content-Type: application/json")
    @POST("/animes")
    fun addAnime(@Body anime: Anime): Call<Anime>
    @PATCH("/animes/{animeId}")
    fun updateAnime(@Path ("animeId") _id:String, @Body anime: Anime): Call<Anime>
    @DELETE("/animes/{animeId}")
    fun deleteAnime(@Path ("animeId") _id: String): Call<Anime>
}