package com.example.animeplacenavi.Home.Presenter

import android.content.Intent
import android.widget.Button
import com.example.animeplacenavi.Helper.GravitySnapHelper
import com.example.animeplacenavi.Home.Helper.AnimeAdapter
import com.example.animeplacenavi.Home.Helper.NewsAdapter
import com.example.animeplacenavi.Home.Model.Anime

interface HomeContact {

    interface Model {
        fun loadResponse(presenter: HomePresenter)
        fun deleteSelectedAnime(_id: String, presenter: HomePresenter)
    }

    interface Presenter {
        fun checkNetworkStatus(status: String)
        fun callJson()
        fun getAnimeResultSuccess(list : List<Anime>?)
        fun getAnimeResultFailure(error: Throwable)
        fun getScrollPosition(position: Int, adapter: NewsAdapter)
        fun getAnimeId(_id: String, title: String, year: String, image: String)
        fun deleteSelectedAnime(_id: String)
        fun deletedMessage(message: String)
        fun getAnimeTitle(title: String)
        fun checkUserType(userType: String)
        fun buttonVisible(editAnimeBtn: Button?, deleteAnimeBtn: Button?)
        fun animeFilter(title: String, animeList: List<Anime>?, animeAdapter: AnimeAdapter?)
    }

    interface View {
        fun onAnimeResultSuccess(list: List<Anime>?)
        fun onAnimeResultFailure(error: Throwable)
        fun onScrollToPosition(point: GravitySnapHelper)
        fun onNetworkStatusResult(status: String)
        fun refreshAnimeListRecyclerView(message: String)
        fun moveToEditAnimePage(intent: Intent)
        fun moveToAnimeAnimeNaviMapPage(intent: Intent)
    }
}