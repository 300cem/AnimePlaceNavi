package com.example.animeplacenavi.Home.View

import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.animeplacenavi.Helper.GravitySnapHelper
import com.example.animeplacenavi.Home.Helper.AnimeAdapter
import com.example.animeplacenavi.Home.Helper.NewsAdapter
import com.example.animeplacenavi.Home.Model.Anime
import com.example.animeplacenavi.Home.Model.AnimeRepository
import com.example.animeplacenavi.Home.Presenter.HomeContact
import com.example.animeplacenavi.Home.Presenter.HomePresenter
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.anime_list_row.*
import kotlinx.android.synthetic.main.anime_list_row.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.search_view.*

class HomeFragment : Fragment(), HomeContact.View {

    private var presenter: HomePresenter? = null
    private var animeRecyclerView: RecyclerView? = null
    private var animeNewsRecyclerView: RecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        presenter = this.context?.let { HomePresenter(it, this, AnimeRepository()) }
        // convert json data to gson format
        val homeFragment = inflater.inflate(R.layout.fragment_home, container, false)
        animeRecyclerView = homeFragment.findViewById(R.id.animeRecyclerView)
        animeNewsRecyclerView = homeFragment.findViewById(R.id.animeNewsRecyclerView)
        val mPrefs = this.context?.getSharedPreferences("userTypeValue",0)
        val userType = mPrefs?.getString("userType","").toString()
        presenter?.checkUserType(userType)

        return homeFragment
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // call MainActivity checkNetworkConnectionStatus function
        (activity as MainActivity).checkNetworkConnectionStatus()
        val statusResult = (activity as MainActivity).checkNetworkConnectionStatus()
        presenter?.checkNetworkStatus(statusResult)
    }

    override fun onAnimeResultSuccess(list: List<Anime>?) {
        // add a single line between every item
        val itemDecoration = DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
        val mDrawable = this.context?.let { ContextCompat.getDrawable(it, R.drawable.divider) }
        mDrawable?.let { itemDecoration.setDrawable(it) }
        animeRecyclerView?.addItemDecoration(itemDecoration)
        animeRecyclerView?.hasFixedSize()
        val linearLayoutManager = LinearLayoutManager(this.context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        animeRecyclerView?.layoutManager = linearLayoutManager
        val animeAdapter = list?.let { presenter?.let { homepresenter -> AnimeAdapter(it, homepresenter) } }
        animeRecyclerView?.adapter = animeAdapter
        searchView.addTextChangedListener {
            presenter?.animeFilter(it.toString(), list, animeAdapter)
        }
        animeNewsRecyclerView?.hasFixedSize()
        val animeNewsLinearLayoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        animeNewsLinearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        animeNewsRecyclerView?.layoutManager = animeNewsLinearLayoutManager
        val animeNewsAdapter =  list?.let { presenter?.let { homepresenter -> NewsAdapter(it, homepresenter) } }
        animeNewsRecyclerView?.adapter = animeNewsAdapter

        animeNewsRecyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val scrollPosition = animeNewsLinearLayoutManager.findFirstVisibleItemPosition()
                if (animeNewsAdapter != null) {
                    presenter?.getScrollPosition(scrollPosition, animeNewsAdapter)
                }
            }
        })
    }

    override fun onAnimeResultFailure(error: Throwable) {
       Toast.makeText(this.context, "system error , $error" , Toast.LENGTH_SHORT).show()
    }

    override fun onScrollToPosition(customSnapHelper: GravitySnapHelper) {
        customSnapHelper.attachToRecyclerView(animeNewsRecyclerView)
    }

    override fun onNetworkStatusResult(status: String) {
        presenter?.callJson()
    }

    override fun refreshAnimeListRecyclerView(message: String) {
        val ft: FragmentTransaction = fragmentManager!!.beginTransaction()
        ft.detach(this).attach(this).commit()
        Toast.makeText(this.context, message, Toast.LENGTH_LONG).show()
    }

    override fun moveToEditAnimePage(intent: Intent) {
        startActivity(intent)
    }

    override fun moveToAnimeAnimeNaviMapPage(intent: Intent) {
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.callJson()
    }
}