package com.example.animeplacenavi.Home.Helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.animeplacenavi.Home.Model.Anime
import com.example.animeplacenavi.Home.Presenter.AnimeContact
import com.example.animeplacenavi.Home.Presenter.AnimePresenter
import com.example.animeplacenavi.Home.Presenter.HomeContact
import com.example.animeplacenavi.Home.Presenter.HomePresenter
import com.example.animeplacenavi.R
import kotlinx.android.synthetic.main.anime_list_row.view.*

class AnimeAdapter (private var animeList: List<Anime>, private val homePresenter: HomeContact.Presenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       val animeView = LayoutInflater.from(parent.context).inflate(R.layout.anime_list_row, parent, false)
        return AnimeRouteViewHolder(animeView, homePresenter)
    }

    override fun getItemCount(): Int = animeList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        animeList[position]?.let { (holder as AnimeRouteViewHolder).bind(it) }
    }

    fun filterAnime(filteredList: List<Anime>) {
        animeList = filteredList
        notifyDataSetChanged()
    }

    class AnimeRouteViewHolder(animeView: View, private val homePresenter: HomeContact.Presenter) : RecyclerView.ViewHolder(animeView) {
        fun bind(anime: Anime) {
            itemView.animeImage.load(anime.image)
            itemView.title.text = anime.title
            itemView.deleteAnimeBtn.setOnClickListener {
                println("anime _id = ${anime._id}")
                homePresenter.deleteSelectedAnime(anime._id)
            }
            itemView.editAnimeBtn.setOnClickListener {
                println("animeId = ${anime._id} ,animeTitle = ${anime.title} , animeYear=${anime.year}, animeImageUrl = ${anime.image}")
                homePresenter.getAnimeId(anime._id, anime.title, anime.year, anime.image)
            }
            itemView.setOnClickListener {
                println("anime title = ${anime.title}")
                homePresenter.getAnimeTitle(anime.title)
            }
            homePresenter.buttonVisible(itemView.editAnimeBtn, itemView.deleteAnimeBtn)
        }
    }
}