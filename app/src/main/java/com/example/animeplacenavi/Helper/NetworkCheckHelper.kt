package com.example.animeplacenavi.Helper

import android.net.*
import android.os.Build

class NetworkCheckHelper {
    val NET_WORK_CONNECTED = "Network connected"
    val NET_WORK_NOT_CONNECTED = "Network not connected"
    var STATUS: String = NET_WORK_NOT_CONNECTED

    fun checkNetworkConnectionStatus(activeInfo: NetworkInfo?, wifiConnected: Boolean, mobileConnected: Boolean) : String {
        if (activeInfo != null && activeInfo.isConnected) {
            if (wifiConnected || mobileConnected) STATUS = NET_WORK_CONNECTED
        }
        return STATUS
    }

    fun observeNetworkStatus(connectivityManager: ConnectivityManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(object :
                ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    println("update status onAvailable: $network")
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    println("update status onLost: $network")
                }
            })
        }
    }
}