package com.example.animeplacenavi

import android.content.Context
import android.content.Intent

import com.example.animeplacenavi.Helper.GravitySnapHelper
import com.example.animeplacenavi.Home.Helper.NewsAdapter
import com.example.animeplacenavi.Home.Model.Anime
import com.example.animeplacenavi.Home.Model.AnimeRepository
import com.example.animeplacenavi.Home.Presenter.HomeContact
import com.example.animeplacenavi.Home.Presenter.HomePresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test

class HomePresenterTest {

    lateinit var presenter: HomePresenter

    @RelaxedMockK
    lateinit var mView: HomeContact.View

    @RelaxedMockK
    lateinit var repository: AnimeRepository

    @RelaxedMockK
    lateinit var mContext: Context

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = HomePresenter(mContext, mView, repository)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    // checkNetworkStatus
    @Test
    fun testCheckNetworkStatus_caseConnected() {
        // Given
        val mockStatus = "Network connected"
        // When
        presenter.checkNetworkStatus(mockStatus)
        // Then
        verify {
            mView.onNetworkStatusResult(mockStatus)
        }
        assertEquals(mockStatus, "Network connected")
    }

    @Test
    fun testCheckNetworkStatus_caseNotConnected() {
        // Given
        val mockStatusIsFailed = "Network not connected"
        // When
        presenter.checkNetworkStatus(mockStatusIsFailed)
        // Then
        verify(exactly = 0) {
            mView.onNetworkStatusResult(mockStatusIsFailed)
        }
//        assertEquals(mockStatusIsFailed, "Network not connected")
    }

    // callJson
    @Test
    fun testCallJson() {
        // When
        presenter.callJson()
        // Then
        verify {
            repository.loadResponse(presenter)
        }
    }

    // getAnimeResultSuccess
    @Test
    fun testGetAnimeResultSuccess() {
        // Given
        val animeList =
            listOf(Anime("1", "K-ON", "2008", "stub.com"), Anime("2", "K-ON", "2008", "stub.com"))
        // When
        presenter.getAnimeResultSuccess(animeList)
        // Then
        verify {
            mView.onAnimeResultSuccess(animeList)
        }
    }

    // getAnimeResultFailure
    @Test
    fun testGetAnimeResultFailure() {
        // Given
        val error = Throwable("cannot connect server", Throwable().cause)
        // When
        presenter.getAnimeResultFailure(error)
        // Then
        verify {
            mView.onAnimeResultFailure(error)
        }
    }

    // getScrollPosition
    @Test
    fun testScrollPosition_caseGravityStart() {
        // Given
        val scrollPosition = 0
        val list = mockk<List<Anime>>()
        val captureGravity = slot<GravitySnapHelper>()
        // When
        presenter.getScrollPosition(scrollPosition, NewsAdapter(list, presenter))
        verify {
            mView.onScrollToPosition(capture(captureGravity))
        }
    }

    @Test
    fun testScrollPosition_caseGravityEnd() {
        // Given
        val animeList =
            listOf(Anime("1", "K-ON", "2008", "stub.com"), Anime("2", "K-ON", "2008", "stub.com"))
        val newsAdapter = NewsAdapter(animeList, presenter)
        val scrollPosition = newsAdapter.itemCount - 1
        val captureGravity = slot<GravitySnapHelper>()
        // When
        presenter.getScrollPosition(scrollPosition, newsAdapter)
        verify {
            mView.onScrollToPosition(capture(captureGravity))
        }
    }

    // getAnimeId
    @Test
    fun getAnimeId() {
        // Given
        val captorIntent = slot<Intent>()
        val animeId = "1"
        val animeTitle = "K-ON"
        val animeYear = "2008"
        val animeImage ="https://upload.wikimedia.org/wikipedia/zh/thumb/d/df/K-On%21%21_anime_offical_poster.jpg/220px-K-On%21%21_anime_offical_poster.jpg"
        presenter.getAnimeId(animeId, animeTitle, animeYear, animeImage)
        // Then
        verify {
            mView.moveToEditAnimePage(capture(captorIntent))
        }
        assertEquals(
            captorIntent.captured.toString(),
            "Intent { cmp=com.example.animeplacenavi/.Home.View.UpdateAnimeActivity (has extras) }"
        )
        assertEquals(captorIntent.captured.extras?.getString("animeId"), "1")
    }

    // deleteSelectedAnime
    @Test
    fun testDeleteSelectedAnime() {
        // Given
        val animeId = "1"
        // When
        presenter.deleteSelectedAnime(animeId)
        // Then
        verify {
            repository.deleteSelectedAnime(animeId, presenter)
        }
    }

    // deletedMessage
    @Test
    fun testDeletedMessage() {
        // Given
        val message = "An anime is deleted"
        // When
        presenter.deletedMessage(message)
        // Then
        verify {
            mView.refreshAnimeListRecyclerView(message)
        }
        assertEquals(message, "An anime is deleted")
    }

    // getAnimeTitle
    @Test
    fun getAnimeTitle() {
        // Given
        val captorIntent = slot<Intent>()
        val animeTitle = "K-ON"
        // When
        presenter.getAnimeTitle(animeTitle)
        // Then
        verify {
            mView.moveToAnimeAnimeNaviMapPage(capture(captorIntent))
        }
        assertEquals(captorIntent.captured.extras?.getString("naviTitle"), "K-ON")
        assertEquals(captorIntent.captured.toString(), "Intent { cmp=com.example.animeplacenavi/.GoogleNavi.View.AnimeMapNaviActivity (has extras) }")
    }
}
